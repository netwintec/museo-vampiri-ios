﻿using CoreLocation;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Json;
using UIKit;
using ObjCRuntime;

namespace MuseoVampiri
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        CLLocationManager locationmanager;
        NSUuid beaconUUID;
        CLBeaconRegion beaconRegion;
        const string beaconId = "BlueBeacon";
        const string uuid = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";

        public bool FirstScan = false;
        public bool LockFirstScan = false;
        public bool TokenGood = false;
        public bool ScanBeIn = false;
        public int StanzaFind = 0;
        public bool Download = false;
        public bool ErrorDownload = false;
        public string PushToken;

        int porta = 82, countSleep = 51, countBeIn = 0;

        bool notification;
        string desc, img;

        public bool SpecialBeacon = false;
        public string SpecialBeaconValue = "";

        Dictionary<int, string> BeaconMusei = new Dictionary<int, string>();
        public Dictionary<int, Stanza> StanzeDictionary = new Dictionary<int, Stanza>();
        Dictionary<int, bool> StanzeFlagDictionary = new Dictionary<int, bool>();
        private static Dictionary<string, List<int>> ListBeaconBeIn = new Dictionary<string, List<int>>();
        public static List<string> BeaconNetworks = new List<string>();

        public bool playingVideo = false;

        public override UIWindow Window
        {
            get;
            set;
        }

        public static AppDelegate Instance { get; private set; }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method

            UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);

            AppDelegate.Instance = this;

            BeaconMusei.Add(102, "lucca");
            BeaconMusei.Add(109, "vampiri");

            startBeacon();

            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, [Transient] UIWindow forWindow)
        {

            if (playingVideo)
                return UIInterfaceOrientationMask.Landscape;
            else
                return UIInterfaceOrientationMask.Portrait;


            //return base.GetSupportedInterfaceOrientations(application, forWindow);
        }

        public void startBeacon()
        {

            beaconUUID = new NSUuid(uuid);
            beaconRegion = new CLBeaconRegion(beaconUUID, beaconId);


            beaconRegion.NotifyEntryStateOnDisplay = true;
            beaconRegion.NotifyOnEntry = true;
            beaconRegion.NotifyOnExit = true;

            locationmanager = new CLLocationManager();


            Console.WriteLine("Beacons");


            locationmanager.RegionEntered += (object sender, CLRegionEventArgs e) =>
            {
                Console.WriteLine("Region Entered");
                /*FirstScan = true;

                if (LockFirstScan)
                {
                    InvokeOnMainThread(() => {
                        //FragmentHome.Self.DownloadBeacon(true);
                    });
                    Download = true;
                }*/

            };

            locationmanager.RegionLeft += (object sender, CLRegionEventArgs e) =>
            {
                Console.WriteLine("EXIT:" + e.ToString());
                InvokeOnMainThread(() => {
                    //FragmentHome.Self.DownloadBeacon(false);
                });
                Download = false;

            };

            locationmanager.DidRangeBeacons += (object sender, CLRegionBeaconsRangedEventArgs e) =>
            {
                if (e.Beacons.Length > 0)
                {
                    FirstScan = true;

                    if (LockFirstScan && !Download)
                    {
                        //HomePage.Instance.DownloadBeacon(true);
                        StanzaFind=1;
                        ViewController.Instance.CreatePaginaBeacons();
                        Download = true;
                    }
                }

                if (e.Beacons.Length == 0 && Download)
                {
                    //HomePage.Instance.DownloadBeacon(false);
                    ViewController.Instance.BackToNoBeacon();
                    ErrorDownload =false;
                    LockFirstScan = false;
                    Download = false;
                }

                Console.WriteLine("ce n'è " + e.Beacons.Length);
                if (FirstScan && TokenGood)
                {
                    if (!LockFirstScan)
                    {
                        var s2 = e.Beacons;
                        int major = -1;
                        foreach (CLBeacon beacon in s2)
                        {
                            major = (int)beacon.Major;
                            break;
                            //Console.WriteLine ("Range Beacon "+beacon.Major+":"+beacon.Minor+" ---> "+beacon.Rssi+"Rssi"); 
                        }
                        if (major >= 0)
                        {

                            LockFirstScan = true;
                            Console.WriteLine("CHIEDERE DATI PER IL MUSEO CON MAJOR " + major);

                            //******** CHIAMATA SALVARE DATI E POI METTERE UNA VARIABILE BOOL A TRUE PER INIZIARE SCAN DATI
                            //102 --> lucca
                            //104 --> san marino
                            //105 --> siena

                            if (major == 109)
                            {
                                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                                var requestN4U = new RestRequest("active-messages", Method.GET);
                                requestN4U.AddHeader("content-type", "application/json");
                                requestN4U.AddHeader("Net4U-Company", "museotortura");
                                requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoVampiriN4U"));

                                IRestResponse response = client.Execute(requestN4U);

                                Console.WriteLine("Result:" + response.StatusCode + "|" + response.Content);

                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {

                                    string museo = BeaconMusei[major];
                                    Console.WriteLine("MUSEO:" + museo);

                                    var lang = NSLocale.PreferredLanguages[0].Substring(0, 2);

                                    StanzeDictionary.Clear();
                                    StanzeFlagDictionary.Clear();

                                    JsonUtility ju = new JsonUtility();
                                    ju.SpacchettamentoJsonBeacon(museo, StanzeDictionary, StanzeFlagDictionary, response.Content, lang);

                                    if (StanzeDictionary.Count != 0)
                                    {
                                        InvokeOnMainThread(() =>
                                        {

                                            StanzaFind = 1;
                                            ViewController.Instance.CreatePaginaBeacons();
                                            //HomePage.Instance.DownloadBeacon(true);
                                        });
                                    }
                                    else {
                                        ErrorDownload = true;
                                    }

                                    Download = true;
                                }
                                else
                                {

                                    Download = false;

                                }
                            }
                        }
                        //JsonUtility ju = new JsonUtility ();
                        //StreamReader strm = new StreamReader (Activity.Assets.Open ("Beacon.json"));
                        //ju.SpacchettamentoJsonBeacon ("lucca", BeaconDictionary, strm, lang.Country);
                    }
                    else if (!ErrorDownload)
                    {
                        SpecialBeacon = false;
                        Console.WriteLine("BeIn:" + countBeIn + "   " + countSleep);
                        if (countSleep > 10)
                        {
                            if (e.Beacons.Length > 0)
                            {
                                var s = e.Beacons;
                                foreach (CLBeacon beacon in s)
                                {
                                    if (beacon.Major.ToString() == "109" && beacon.Rssi >= -99)
                                    {
                                        SpecialBeacon = true;
                                        SpecialBeaconValue = "109";
                                        countBeIn = 4;
                                    }
                                            
                                }

                                countBeIn++;
                            }
                            else
                            {
                                countBeIn = 0;
                            }
                            if (countBeIn == 5)
                            {

                                if (SpecialBeacon && SpecialBeaconValue == "109")
                                {
                                    Stanza st = StanzeDictionary[1];
                                    bool flagSt = StanzeFlagDictionary[1];

                                    if (!flagSt)
                                    {

                                        Console.WriteLine("Ti trovi nel museo di " + st.museo + " nella stanza " + st.stanza);
                                        StanzaFind = 1;
                                        StanzeFlagDictionary[1] = true;


                                        InvokeOnMainThread(() =>
                                        {


                                            StanzaFind = 1;
                                            ViewController.Instance.CreatePaginaBeacons();

                                        });
                                    }
                                }

                                countBeIn = 0;
                                countSleep = 0;
                            }
                        }
                        else
                        {
                            countSleep++;
                        }
                        
                    }
                }
            };

            Console.WriteLine(UIDevice.CurrentDevice.SystemVersion + "|" + UIDevice.CurrentDevice.CheckSystemVersion(8, 0));
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                locationmanager.RequestAlwaysAuthorization();
            locationmanager.StartMonitoring(beaconRegion);
            locationmanager.StartRangingBeacons(beaconRegion);



        }

        public bool IsPointInPolygon(List<Location> poly, Location point)
        {
            int i, j;
            bool c = false;

            Console.WriteLine("VALORE" + point.Lt + "|" + point.Lg);

            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {

                if ((((poly[i].Lt <= point.Lt) && (point.Lt < poly[j].Lt))
                    || ((poly[j].Lt <= point.Lt) && (point.Lt < poly[i].Lt)))
                    && (point.Lg < (poly[j].Lg - poly[i].Lg) * (point.Lt - poly[i].Lt)
                        / (poly[j].Lt - poly[i].Lt) + poly[i].Lg))
                {

                    c = !c;
                    //Console.WriteLine ("Prima:"+!c +"|Dopo:"+c);
                }
            }

            Console.WriteLine("Final:" + c);
            return c;
        }
    }
}