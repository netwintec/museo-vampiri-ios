﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;
using System.Json;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MuseoVampiri
{		
	public class UtilityLoginManager  
	{
		bool flagNome=false,flagCognome=false,flagEMail=false,flagPW=false;
		int porta =82;

		public UtilityLoginManager() { }

		public List<string> Login(string Email, string pass)
		{
			flagPW = false;
			flagEMail = false;

			if (pass == null || pass == "")
			{
				flagPW = true;
			}
			if (Email == null || Email == "")
			{
				flagEMail = true;
			}
			else
			{
				try
				{
					MailAddress m = new MailAddress(Email);
				}
				catch (FormatException)
				{
					flagEMail = true;
				}
			}

			if (flagPW == true || flagEMail == true)
			{

				List<string> content = new List<string>();
				content.Add("ERRORDATA");
				return content;

			}


			var client = new RestClient("http://api.netwintec.com:" + porta + "/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U = new RestRequest("login/customer", Method.POST);
			requestN4U.AddHeader("content-type", "application/json");
			requestN4U.AddHeader("Net4U-Company", "museotortura");
			requestN4U.Timeout = 60000;

			JObject oJsonObject = new JObject();

			oJsonObject.Add("email", Email);
			oJsonObject.Add("password", pass);

			requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


			IRestResponse response = client.Execute(requestN4U);



			Console.WriteLine("Result:" + response.Content);

			if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
			{

				List<string> content = new List<string>();
				content.Add("ERROR");
				return content;

			}

			if (response.StatusCode == System.Net.HttpStatusCode.OK)
			{

				List<string> content = new List<string>();
				content.Add("SUCCESS");

				JsonValue json = JsonValue.Parse(response.Content);
				//Console.WriteLine (response.Content);
				content.Add(json["token"]);

				return content;

			}

			List<string> contentErr = new List<string>();
			contentErr.Add("ERROR");
			return contentErr;

		}
	}
}