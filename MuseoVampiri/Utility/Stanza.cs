﻿using System;
using System.Collections.Generic;

namespace MuseoVampiri
{
	public class Stanza
	{
		public List <Location> Poligono = new List<Location>();
		public Dictionary <int,BeaconInfo> BeaconDictionary = new Dictionary <int,BeaconInfo>();
		public int piano,stanza;
		public string museo, img_file;

		public Stanza ()
		{
		}
	}
}

