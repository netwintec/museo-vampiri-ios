﻿using AVFoundation;
using Carousels;
using CoreGraphics;
using CoreMedia;
using FFImageLoading;
using FFImageLoading.Work;
using Foundation;
using MediaPlayer;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UIKit;

namespace MuseoVampiri
{
    public partial class ViewController : UIViewController, IAVAudioPlayerDelegate
    {

        UIView Back;

        public UIView ViewVideo, ViewMain, ViewSito;
        public iCarousel carousel;

        public UIView ViewStanze, ViewBeacons, ViewInfoBeacon;

        public bool isNOBEAC = false;
        public bool isSTANZA = false;
        public bool isBEAC = false;
        public bool isInfoBEAC = false;

        UIImage play_NA = UIImage.FromFile("play.png");
        UIImage play_A = UIImage.FromFile("play_attivo.png");
        UIImage pause_NA = UIImage.FromFile("pause.png");
        UIImage pause_A = UIImage.FromFile("pause_attivo.png");
        UIImage stopImm = UIImage.FromFile("stop.png");

        AVAudioPlayer audioPlayer;
        bool IsPlaying;

        MPMoviePlayerController player;
        bool isPlay;

        AVPlayer _player;

        // MPMoviePlayerController playerVideo;
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public static ViewController Instance
        {
            private set;
            get;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            /*********** FONT APP ************

            Montserrat-Regular
            Montserrat-Light
            Montserrat-Bold
            Montserrat-Medium

            **********************************/

            ViewController.Instance = this;

            //**** NAV BAR VIEW *********

            UIView NavBar = new UIView(new CGRect(0, 0, View.Frame.Width, 70));
            NavBar.BackgroundColor = UIColor.Black;

            Back = new UIView(new CGRect(NavBar.Frame.Width-50,20,50,50));
            Back.Alpha = 0;
            UITapGestureRecognizer tapBack = new UITapGestureRecognizer(() => {

                BackClick();

            });
            Back.UserInteractionEnabled = true;
            Back.AddGestureRecognizer(tapBack);
  
            UIImageView backImg = new UIImageView(new CGRect(12.5,15.5,25,19));
            backImg.Image = UIImage.FromFile("back.png");
            Back.Add(backImg);

            UIImageView logo = new UIImageView(new CGRect(View.Frame.Width / 2 - 13.5, 25, 27, 40));
            logo.Image = UIImage.FromFile("Logo.png");

            NavBar.Add(Back);
            NavBar.Add(logo);

            View.Add(NavBar);

            //***************************



            //**** BOTTOM VIEW *********

            UIView BottomView = new UIView(new CGRect(0, View.Frame.Height - 80, View.Frame.Width, 80));
            BottomView.BackgroundColor = UIColor.White;

            UIView Sep = new UIView(new CGRect(0, 0, View.Frame.Width, 1));
            Sep.BackgroundColor = UIColor.FromRGB(160,0,27);
            BottomView.Add(Sep);

            float terzoPagina = (float)View.Frame.Width / 3;

            UIImageView ImgVideoSp = new UIImageView(new CGRect(terzoPagina / 2 - 20, 26.5, 40, 27));
            ImgVideoSp.Image = UIImage.FromFile("VideoIcon.png");

            UITapGestureRecognizer tapV = new UITapGestureRecognizer(() =>
            {

                carousel.CurrentItemIndex = 0;

            });
            ImgVideoSp.UserInteractionEnabled = true;
            ImgVideoSp.AddGestureRecognizer(tapV);

            BottomView.Add(ImgVideoSp);

            UIImageView ImgMainSp = new UIImageView(new CGRect(terzoPagina+terzoPagina / 2 - 20, 26.5, 40, 27));
            ImgMainSp.Image = UIImage.FromFile("AudioIcon.png");
            ImgMainSp.Alpha = 0;

            UITapGestureRecognizer tapM = new UITapGestureRecognizer(() =>
            {

                carousel.CurrentItemIndex = 1;

            });
            ImgMainSp.UserInteractionEnabled = true;
            ImgMainSp.AddGestureRecognizer(tapM);

            BottomView.Add(ImgMainSp);

            UIImageView ImgSitoSp = new UIImageView(new CGRect(terzoPagina * 2 + terzoPagina / 2 - 20, 26.5, 40, 27));
            ImgSitoSp.Image = UIImage.FromFile("WebIcon.png");

            UITapGestureRecognizer tapS = new UITapGestureRecognizer(() =>
            {

                carousel.CurrentItemIndex = 2;

            });
            ImgSitoSp.UserInteractionEnabled = true;
            ImgSitoSp.AddGestureRecognizer(tapS);

            BottomView.Add(ImgSitoSp);

            string Video = NSBundle.MainBundle.LocalizedString("Home_Video", "Trailer", null);
            string Main = NSBundle.MainBundle.LocalizedString("Home_AudioG", "Audioguide", null);
            string Web = NSBundle.MainBundle.LocalizedString("Home_Web", "Web", null);

            var sizeV = UIStringDrawing.StringSize(
                Video,
                UIFont.FromName("Montserrat-Light", 13),
                new CGSize(terzoPagina,40));

            var sizeM = UIStringDrawing.StringSize(
                Main,
                UIFont.FromName("Montserrat-Light", 13),
                new CGSize(terzoPagina, 40));

            var sizeW = UIStringDrawing.StringSize(
                Web,
                UIFont.FromName("Montserrat-Light", 13),
                new CGSize(terzoPagina, 40));

            UIImageView ImgVideoAc = new UIImageView(new CGRect(terzoPagina / 2 - 20, 40 - ((sizeV.Height + 32)/2), 40, 27));
            ImgVideoAc.Image = UIImage.FromFile("VideoIcon.png");
            ImgMainSp.Alpha = 0;
            BottomView.Add(ImgVideoAc);

            UILabel TxtVideoAc = new UILabel(new CGRect(terzoPagina / 2 - sizeV.Width /2, ImgVideoAc.Frame.Y+32, sizeV.Width, sizeV.Height));
            TxtVideoAc.Text = Video;
            TxtVideoAc.Font = UIFont.FromName("Montserrat-Light", 13);
            TxtVideoAc.TextColor = UIColor.FromRGB(160, 0, 27);
            TxtVideoAc.Alpha = 0;
            BottomView.Add(TxtVideoAc);


            UIImageView ImgMainAC = new UIImageView(new CGRect(terzoPagina + terzoPagina / 2 - 20, 40 - ((sizeV.Height + 32) / 2), 40, 27));
            ImgMainAC.Image = UIImage.FromFile("AudioIcon.png");
            BottomView.Add(ImgMainAC);

            UILabel TxtMainAc = new UILabel(new CGRect(terzoPagina + terzoPagina / 2 - sizeM.Width / 2, ImgVideoAc.Frame.Y + 32, sizeM.Width, sizeM.Height));
            TxtMainAc.Text = Main;
            TxtMainAc.Font = UIFont.FromName("Montserrat-Light", 13);
            TxtMainAc.TextColor = UIColor.FromRGB(160, 0, 27);
            BottomView.Add(TxtMainAc);


            UIImageView ImgSitoAc = new UIImageView(new CGRect(terzoPagina * 2 + terzoPagina / 2 - 20, 40 - ((sizeV.Height + 32) / 2), 40, 27));
            ImgSitoAc.Image = UIImage.FromFile("WebIcon.png");
            ImgSitoAc.Alpha = 0;
            BottomView.Add(ImgSitoAc);

            UILabel TxtSitoAc = new UILabel(new CGRect(terzoPagina * 2 + terzoPagina / 2 - sizeW.Width / 2, ImgVideoAc.Frame.Y + 32, sizeW.Width, sizeW.Height));
            TxtSitoAc.Text = Web;
            TxtSitoAc.Font = UIFont.FromName("Montserrat-Light", 13);
            TxtSitoAc.TextColor = UIColor.FromRGB(160, 0, 27);
            TxtSitoAc.Alpha = 0;
            BottomView.Add(TxtSitoAc);


            View.Add(BottomView);

            //**************************


            //**** VIDEO VIEW *********

            ViewVideo = new UIView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 150));
            ViewVideo.BackgroundColor = UIColor.Black;

            string VideoTxt = NSBundle.MainBundle.LocalizedString("Home_GuardaVideo", "Watch Trailer", null);

            var sizeVTxt = UIStringDrawing.StringSize(
                VideoTxt,
                UIFont.FromName("Montserrat-Light", 18),
                new CGSize(ViewVideo.Frame.Width-40, 2000));

            UILabel TxtVideo = new UILabel(new CGRect(ViewVideo.Frame.Width / 2 - sizeVTxt.Width / 2, ViewVideo.Frame.Height/2 -(sizeVTxt.Height + 75)/2, sizeVTxt.Width, sizeVTxt.Height));
            TxtVideo.Text = VideoTxt;
            TxtVideo.Font = UIFont.FromName("Montserrat-Light", 18);
            TxtVideo.TextColor = UIColor.FromRGB(160, 0, 27);
            ViewVideo.Add(TxtVideo);

            UIImageView ImgVideo = new UIImageView(new CGRect(ViewVideo.Frame.Width / 2 - 35, TxtVideo.Frame.Y + TxtVideo.Frame.Height +5, 70, 70));
            //ImgVideo.Text = "ahahaha";
            ImgVideo.Image = UIImage.FromFile("PlayVideo.png");
            ViewVideo.Add(ImgVideo);

            Console.WriteLine("Video View:" + ViewVideo.Frame+
                "\nTxtVideo:" + TxtVideo.Frame +
                "\nImgVideo:" + ImgVideo.Frame+
                "\n VW count s:"+ ViewVideo.Subviews.Length);

            // VIDEO TRAILER

            UIView Trailer = new UIView(new CGRect(View.Frame.Width / 2 - View.Frame.Height / 2, View.Frame.Height / 2 - View.Frame.Width / 2, View.Frame.Height, View.Frame.Width));
            Trailer.BackgroundColor = UIColor.Black;

            var _asset = AVAsset.FromUrl(NSUrl.FromFilename("trailer.mp4"));
            var _playerItem = new AVPlayerItem(_asset);

            _player = new AVPlayer(_playerItem);
            
            var _playerLayer = AVPlayerLayer.FromPlayer(_player);
            _playerLayer.Frame = new CGRect(20,50,View.Frame.Height-20,View.Frame.Width-50);

            UIView SlidBack = new UIView(new CGRect(20,0, View.Frame.Height-20,50));
            SlidBack.BackgroundColor = UIColor.Black;//FromRGBA(255,255,255,100);

            UIView SlidSep = new UIView(new CGRect(0, 49, View.Frame.Height - 20, 1));
            SlidSep.BackgroundColor = UIColor.White;
            SlidBack.AddSubview(SlidSep);

            /*UILabel esc = new UILabel(new CGRect(SlidBack.Frame.Width-50,5,40,20));
            esc.Text = "ciao";
            esc.TextColor = UIColor.Purple;
            esc.BackgroundColor = UIColor.Yellow;*/

            UIImageView esc = new UIImageView(new CGRect(12.5, 15.5, 25, 19));
            esc.Image = UIImage.FromFile("back.png");

            UIView escBtn = new UIView(new CGRect(SlidBack.Frame.Width - 50, 0, 50, 50));
            UITapGestureRecognizer tapesc = new UITapGestureRecognizer(() => {
                if (_player != null || isPlay || IsPlaying)
                {

                    _player.Pause();
                    Trailer.Alpha = 0;
                }
                

            });
            escBtn.UserInteractionEnabled = true;
            escBtn.AddGestureRecognizer(tapesc);

            UIImageView Slidlogo = new UIImageView(new CGRect(SlidBack.Frame.Width / 2 - 13.5, 5, 27, 40));
            Slidlogo.Image = UIImage.FromFile("Logo.png");

            escBtn.AddSubview(esc);
            SlidBack.AddSubview(escBtn);
            SlidBack.AddSubview(Slidlogo);

            Trailer.Add(SlidBack);

            Trailer.Layer.AddSublayer(_playerLayer);

            Trailer.Layer.AnchorPoint = new CGPoint(0.5, 0.5);
            Trailer.Layer.AffineTransform = CGAffineTransform.MakeRotation((float)Math.PI / 2);

            Trailer.Alpha = 0;

            UITapGestureRecognizer ImgVideoTap = new UITapGestureRecognizer((s) =>
            {
                if (!AppDelegate.Instance.playingVideo)
                {
                    if (_player != null || isPlay || IsPlaying)
                    {

                        Trailer.Alpha = 1;
                        _player.Seek(CMTime.Zero);
                        _player.Play();
                    }
                }
            });
            ImgVideo.UserInteractionEnabled = true;
            ImgVideo.AddGestureRecognizer(ImgVideoTap);

            //************************


            //**** MAIN VIEW *********

            ViewMain = new UIView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 150));
            ViewMain.BackgroundColor = UIColor.Black;

            UIImageView ImgMain = new UIImageView(new CGRect(ViewMain.Frame.Width / 2 - 102.5, ViewMain.Frame.Height / 2 - 75, 205, 150));
            ImgMain.Image = UIImage.FromFile("AuudioGuidaHome.png");
            ViewMain.Add(ImgMain);

            ViewStanze = new UIView(new CGRect(0, 0, ViewMain.Frame.Width, ViewMain.Frame.Height));
            ViewStanze.BackgroundColor = UIColor.Black;
            ViewStanze.Alpha = 0;
            ViewMain.Add(ViewStanze);

            ViewBeacons = new UIView(new CGRect(0, 0, ViewMain.Frame.Width, ViewMain.Frame.Height));
            ViewBeacons.BackgroundColor = UIColor.Black;
            ViewBeacons.Alpha = 0;
            ViewMain.Add(ViewBeacons);

            ViewInfoBeacon = new UIView(new CGRect(0, 0, ViewMain.Frame.Width, ViewMain.Frame.Height));
            ViewInfoBeacon.BackgroundColor = UIColor.Black;
            ViewInfoBeacon.Alpha = 0;
            ViewMain.Add(ViewInfoBeacon);

            isNOBEAC = true;
            isSTANZA = false;
            isBEAC = false;
            isInfoBEAC = false;

            //************************



            //**** SITO VIEW *********

            ViewSito = new UIView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 150));
            ViewSito.BackgroundColor = UIColor.Black;

            UIWebView web = new UIWebView(new CGRect(0, 0, ViewSito.Frame.Width, ViewSito.Frame.Height));
            web.LoadRequest(new NSUrlRequest(new NSUrl("http://www.museodeivampiri.com")));
            web.ScrollView.Bounces = false;
            ViewSito.Add(web);

            //************************

            carousel = new iCarousel(new CGRect(0,70,View.Frame.Width,View.Frame.Height - 150));
            carousel.Type = iCarouselType.Linear;
            carousel.CurrentItemIndex = 1;
            carousel.DecelerationRate = 2.3f;
            carousel.Bounces = false;
            carousel.DataSource = new CarouselDataSource(this);
            carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;


            View.Add(carousel);


            // handle item selections / taps
            carousel.CurrentItemIndexChanged += (sender, args) => {
                var indexSelected = (int)carousel.CurrentItemIndex;


                if (indexSelected == 0)
                {
                    ImgVideoSp.Alpha = 0;

                    ImgMainSp.Alpha = 1;

                    ImgSitoSp.Alpha = 1;

                    ImgVideoAc.Alpha = 1;
                    TxtVideoAc.Alpha = 1;

                    ImgMainAC.Alpha = 0;
                    TxtMainAc.Alpha = 0;

                    ImgSitoAc.Alpha = 0;
                    TxtSitoAc.Alpha = 0;

                    Back.Alpha = 0;

                }

                if (indexSelected == 1)
                {
                    ImgVideoSp.Alpha = 1;

                    ImgMainSp.Alpha = 0;

                    ImgSitoSp.Alpha = 1;

                    ImgVideoAc.Alpha = 0;
                    TxtVideoAc.Alpha = 0;

                    ImgMainAC.Alpha = 1;
                    TxtMainAc.Alpha = 1;

                    ImgSitoAc.Alpha = 0;
                    TxtSitoAc.Alpha = 0;

                    if(isInfoBEAC)
                        Back.Alpha = 1;
                    else
                        Back.Alpha = 0;

                }

                if (indexSelected == 2)
                {
                    ImgVideoSp.Alpha = 1;

                    ImgMainSp.Alpha = 1;

                    ImgSitoSp.Alpha = 0;

                    ImgVideoAc.Alpha = 0;
                    TxtVideoAc.Alpha = 0;

                    ImgMainAC.Alpha = 0;
                    TxtMainAc.Alpha = 0;

                    ImgSitoAc.Alpha = 1;
                    TxtSitoAc.Alpha = 1;

                    Back.Alpha = 0;

                }
                // do something with a selection
            };


            // GESTIONE APERTURA VIDEO

            NSNotificationCenter.DefaultCenter.AddObserver(
                new NSString("MPMoviePlayerWillEnterFullscreenNotification"),
                (notify) => {


                    if (carousel.CurrentItemIndex == 1 && isInfoBEAC)
                    {
                        Console.WriteLine("Will enter full screen");
                        player.Play();
                        isPlay = true;
                        player.ControlStyle = MPMovieControlStyle.Fullscreen;
                    }

                }
            );
            NSNotificationCenter.DefaultCenter.AddObserver(
                new NSString("MPMoviePlayerWillExitFullscreenNotification"),
                (notify) => {


                    if (carousel.CurrentItemIndex == 1 && isInfoBEAC)
                    {
                        player.ControlStyle = MPMovieControlStyle.None;
                        player.Pause();
                        isPlay = false;
                    }
                }
            );

            NSNotificationCenter.DefaultCenter.AddObserver(
                new NSString(AVPlayerItem.DidPlayToEndTimeNotification),
                (notify) => {

                    Trailer.Alpha = 0;
                }
            );




            View.Add(Trailer);

            Login();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override UIStatusBarStyle PreferredStatusBarStyle()
        {
            return UIStatusBarStyle.LightContent;
        }

        //*** LOGIN *****

        public async void Login()
        {

            UtilityLoginManager loginManager = new UtilityLoginManager();
            List<string> result = loginManager.Login("a@a.it", "a");
            Console.WriteLine(result[0]);

            if (result[0] == "SUCCESS")
            {
                InvokeOnMainThread(() => { 
                    Console.WriteLine(result[1]);

                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenMuseoVampiriN4U");

                    AppDelegate.Instance.TokenGood = true;
                });

            }

            if (result[0] == "ERROR")
            {

                int button = await ShowAlert("Sei offline", "Sembra ci siano dei problemi di connessione riprova per collegarti online", "Riprova", "Annulla");
                Console.WriteLine("Click" + button);
                if (button == 0)
                {
                    InvokeOnMainThread(() => {
                        Login();
                    });
                }
                else { }

            }

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        //***************


        // GESTIONE BACK


        public void BackToNoBeacon()
        {

            foreach (var vs in ViewStanze.Subviews)
            {
                vs.RemoveFromSuperview();
            }

            ViewStanze.Alpha = 0;

            foreach (var vb in ViewBeacons.Subviews)
            {
                vb.RemoveFromSuperview();
            }

            ViewBeacons.Alpha = 0;

            foreach (var vib in ViewInfoBeacon.Subviews)
            {
                vib.RemoveFromSuperview();
            }

            ViewInfoBeacon.Alpha = 0;


            if (isInfoBEAC && audioPlayer != null)
            {
                audioPlayer.Dispose();
                audioPlayer = null;
            }

            isNOBEAC = true;
            isSTANZA = false;
            isBEAC = false;
            isInfoBEAC = false;

        }

        public void BackClick()
        {

            /*if (isBEAC)
            {
                foreach (var vb in ViewBeacons.Subviews)
                {
                    vb.RemoveFromSuperview();
                }

                ViewBeacons.Alpha = 0;

                Back.Alpha = 0;

                isNOBEAC = false;
                isSTANZA = true;
                isBEAC = false;
                isInfoBEAC = false;

            }*/

            if (isInfoBEAC)
            {
                foreach (var vib in ViewInfoBeacon.Subviews)
                {
                    vib.RemoveFromSuperview();
                }

                if (audioPlayer != null)
                {
                    audioPlayer.Dispose();
                    audioPlayer = null;
                }

                ViewInfoBeacon.Alpha = 0;

                Back.Alpha = 0;

                isNOBEAC = false;
                isSTANZA = false;
                isBEAC = true;
                isInfoBEAC = false;

            }
            

        }


        // ***********************

        // CREAZIONE PAGINA STANZE
        /*
        public void CreatePaginaStanze() {

            foreach (var v in ViewStanze.Subviews) {
                v.RemoveFromSuperview();
            }

            ViewStanze.Alpha = 1;

            isNOBEAC = false;
            isSTANZA = true;
            isBEAC = false;
            isInfoBEAC = false;

            var StanzeDictionary = AppDelegate.Instance.StanzeDictionary;
            List<UIImageView> listImage = new List<UIImageView>();

            UIScrollView scrollView = new UIScrollView(new CGRect(0, 0, ViewStanze.Frame.Width, ViewStanze.Frame.Height));

            int yy = 0;

            for (int i = 0; i < StanzeDictionary.Count; i++)
            {

                Stanza st = StanzeDictionary[i + 1];

                float ImgH = (float)((ViewStanze.Frame.Width * 1200) / 1900);
                float SfocH = (float)((ViewStanze.Frame.Width * 360) / 1900);

                UIImageView img = new UIImageView();
                img = new UIImageView(new CGRect(0, yy, ViewStanze.Frame.Width, ImgH));

                UIImageView sfoc = new UIImageView();
                sfoc = new UIImageView(new CGRect(0, (yy + ImgH) - SfocH, ViewStanze.Frame.Width, SfocH));
                sfoc.Image = UIImage.FromFile("Sfocatura.png");

                string stanzaTxt = NSBundle.MainBundle.LocalizedString("StanzaH", "ROOM", null) + " " + st.stanza.ToString();
                var size = UIStringDrawing.StringSize(stanzaTxt, UIFont.FromName("Montserrat-Medium", 20), new CGSize(ViewStanze.Frame.Width - 60, 2000));

                UILabel text = new UILabel();
                text = new UILabel(new CGRect(30, (yy + ImgH) - size.Height - 10, ViewStanze.Frame.Width - 60, size.Height));
                text.Font = UIFont.FromName("Montserrat-Medium", 20);
                text.Text = stanzaTxt;
                text.TextColor = UIColor.White;
                text.BackgroundColor = UIColor.Clear;
                text.TextAlignment = UITextAlignment.Center;


                if (st.img_file == "Lucca1")
                    img.Image = UIImage.FromFile("stanzaLucca1.png");
                if (st.img_file == "Lucca2")
                    img.Image = UIImage.FromFile("stanzaLucca2.png");
                if (st.img_file == "Lucca3")
                    img.Image = UIImage.FromFile("stanzaLucca3.png");
                if (st.img_file == "Lucca4")
                    img.Image = UIImage.FromFile("stanzaLucca4.png");
                if (st.img_file == "Lucca5")
                    img.Image = UIImage.FromFile("stanzaLucca5.png");

                UITapGestureRecognizer imgTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < listImage.Count; ii++)
                    {
                        Console.WriteLine((listImage[ii] == s.View) + "|" + ii);
                        if (listImage[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    AppDelegate.Instance.StanzaFind = giusto + 1;
                    CreatePaginaBeacons();

                });
                img.UserInteractionEnabled = true;
                img.AddGestureRecognizer(imgTap);


                listImage.Add(img);
                scrollView.Add(img);
                scrollView.Add(sfoc);
                scrollView.Add(text);

                yy += (int)ImgH;

            }

            scrollView.ContentSize = new CGSize(ViewStanze.Frame.Width, yy + 10);

            ViewStanze.Add(scrollView);

        }*/


        // CREAZIONE PAGINA BEACONS

        public void CreatePaginaBeacons()
        {

            foreach (var v in ViewBeacons.Subviews)
            {
                v.RemoveFromSuperview();
            }

            ViewBeacons.Alpha = 1;

            Back.Alpha = 0;

            isNOBEAC = false;
            isSTANZA = false;
            isBEAC = true;
            isInfoBEAC = false;

            UIScrollView scrollView = new UIScrollView(new CGRect(0, 0, ViewBeacons.Frame.Width, ViewBeacons.Frame.Height));

            //JsonUtility ju = new JsonUtility();
            //ju.SpacchettamentoJsonBeacon("lucca", BeaconDictionary, text, NSLocale.PreferredLanguages[0].Substring(0, 2));

            var BeaconDictionary = AppDelegate.Instance.StanzeDictionary[AppDelegate.Instance.StanzaFind].BeaconDictionary;
            List<UIImageView> listImage = new List<UIImageView>();

            int yy = 0;
            /*
            UIImageView TopImage = new UIImageView(new CGRect(0, 0, View.Frame.Width, 50));
            TopImage.Image = UIImage.FromFile("mostra_sfondo.jpg");

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);

            UILabel TopLabel = new UILabel(new CGRect(0, 10, View.Frame.Width, 40));
            TopLabel.Font = UIFont.FromName("DaunPenh", 32);
            TopLabel.Frame.Inset(0, 5); //= new UIEdgeInsets(5, 0, 0, 0);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            //TopLabel.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("BeaconH_Stanza", "ROOM N°", null) + BeaconDictionary[0].stanza;

            yy += 50;

            var size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("BeaconH_Scopri", "Click to learn more", null), UIFont.FromName("MyriadPro-Regular", 22), new CGSize(ContentView.Frame.Width, 2000));

            UILabel tap = new UILabel(new CGRect(0, yy + 15, ViewBeacons.Frame.Width, size.Height));
            tap.Font = UIFont.FromName("MyriadPro-Regular", 22);
            tap.TextAlignment = UITextAlignment.Center;
            tap.TextColor = UIColor.White;
            tap.Text = NSBundle.MainBundle.LocalizedString("BeaconH_Scopri", "Click to learn more", null);

            yy += (int)(size.Height + 15);
            */
            float x = 30;
            float w = (float)((ViewBeacons.Frame.Width - 90) / 2);
            float h = w;
            //yy += 10;

            for (int i = 0; i < BeaconDictionary.Count; i++)
            {
                UIImageView img = new UIImageView();
                if (i % 2 == 0)
                {
                    img = new UIImageView(new CGRect(x, yy + 15, w, h));
                    img.Image = UIImage.FromFile("placeholder.png");
                    yy += (int)(h + 15);
                }
                if (i % 2 == 1)
                {
                    img = new UIImageView(new CGRect(w + 2 * x, yy - h, w, h));
                    img.Image = UIImage.FromFile("placeholder.png");
                }

                UITapGestureRecognizer imgTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < listImage.Count; ii++)
                    {
                        Console.WriteLine((listImage[ii] == s.View) + "|" + ii);
                        if (listImage[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    if (BeaconDictionary[giusto].tipo == "immagine")
                    {

                        var ObjectSelected = BeaconDictionary[giusto];
                        Console.WriteLine("immagine");
                        CreatePaginaInfoImage(ObjectSelected);

                    }
                    if (BeaconDictionary[giusto].tipo == "immagini")
                    {

                        var ObjectSelected = BeaconDictionary[giusto];
                        Console.WriteLine("immagini");
                        CreatePaginaInfoImages(ObjectSelected);

                    }
                    if (BeaconDictionary[giusto].tipo == "video")
                    {

                        var ObjectSelected = BeaconDictionary[giusto];
                        Console.WriteLine("video");
                        CreatePaginaInfoVideo(ObjectSelected);

                    }

                });
                img.UserInteractionEnabled = true;
                img.AddGestureRecognizer(imgTap);

                ImageService.Instance
                    .LoadUrl(BeaconDictionary[i].url_thumbnail)
                    .LoadingPlaceholder("placeholder.png", ImageSource.CompiledResource)
                    .Into(img);


                listImage.Add(img);
                scrollView.Add(img);

            }

            scrollView.ContentSize = new CGSize(ViewBeacons.Frame.Width, yy + 10);

            ViewBeacons.Add(scrollView);

        }


        // CREAZIONI PAGINE INFO

        public void CreatePaginaInfoImage(BeaconInfo oggetto) {

            foreach (var v in ViewInfoBeacon.Subviews)
            {
                v.RemoveFromSuperview();
            }

            Back.Alpha = 1;

            isNOBEAC = false;
            isSTANZA = false;
            isBEAC = false;
            isInfoBEAC = true;


            audioPlayer = null;

            IsPlaying = false;

            UIImageView play, pause, stop;
            UIView loadView;

            int yy = 0;

            var size = UIStringDrawing.StringSize(oggetto.titolo, UIFont.FromName("Montserrat-Medium", 18), new CGSize(ViewInfoBeacon.Frame.Width, 2000));

            UILabel TitoloLabel = new UILabel(new CGRect(0, 12, ViewInfoBeacon.Frame.Width, size.Height));
            TitoloLabel.Font = UIFont.FromName("Montserrat-Medium", 18);
            TitoloLabel.TextAlignment = UITextAlignment.Center;
            TitoloLabel.TextColor = UIColor.White;
            TitoloLabel.Text = oggetto.titolo;
            TitoloLabel.Lines = 0;

            yy += (int)(size.Height + 15);

            float ImageLato = (float)(((ViewInfoBeacon.Frame.Height - (size.Height + 90)) / 2) - 30);
            float ScrollHeight = (float)(((ViewInfoBeacon.Frame.Height - (size.Height + 90)) / 2) + 30);

            UIImageView image = new UIImageView(new CGRect((ViewInfoBeacon.Frame.Width / 2) - (ImageLato / 2), yy + 10, ImageLato, ImageLato));
            image.Image = UIImage.FromFile("placeholder.png");

            ImageService.Instance
                   .LoadUrl(oggetto.urlImage)
                   .LoadingPlaceholder("placeholder.png", ImageSource.CompiledResource)
                   .Into(image);


            yy += (int)(ImageLato + 10);

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;

            play = new UIImageView(new CGRect(ViewInfoBeacon.Frame.Width / 2 - 70, yy + 11, 35, 35));
            play.Image = play_NA;

            pause = new UIImageView(new CGRect(ViewInfoBeacon.Frame.Width / 2 - 17.5, yy + 11, 35, 35));
            pause.Image = pause_NA;

            stop = new UIImageView(new CGRect(ViewInfoBeacon.Frame.Width / 2 + 35, yy + 11, 35, 35));
            stop.Image = stopImm;

            UITapGestureRecognizer playTap = new UITapGestureRecognizer((s) =>
            {
                if (isHeadsetPluggedIn())
                {

                    if (!IsPlaying && audioPlayer != null)
                    {

                        audioPlayer.Play();
                        play.Image = play_A;
                        pause.Image = pause_NA;
                        IsPlaying = true;

                    }

                }
                else
                {
                    var alert = new UIAlertView("Errore", "Attaccare le cuffie per usufruire dell'audioguida", null, "Ok");
                    alert.Show();
                }
            });
            play.UserInteractionEnabled = true;
            play.AddGestureRecognizer(playTap);

            UITapGestureRecognizer pauseTap = new UITapGestureRecognizer((s) =>
            {

                if (IsPlaying && audioPlayer != null)
                {

                    audioPlayer.Pause();
                    play.Image = play_NA;
                    pause.Image = pause_A;
                    IsPlaying = false;

                }

            });
            pause.UserInteractionEnabled = true;
            pause.AddGestureRecognizer(pauseTap);

            UITapGestureRecognizer stopTap = new UITapGestureRecognizer((s) =>
            {
                if (audioPlayer != null)
                {

                    audioPlayer.Pause();
                    audioPlayer.CurrentTime = 0.0;
                    play.Image = play_NA;
                    pause.Image = pause_NA;
                    IsPlaying = false;

                }
            });
            stop.UserInteractionEnabled = true;
            stop.AddGestureRecognizer(stopTap);


            loadView = new UIView(new CGRect(0, yy + 5, ViewInfoBeacon.Frame.Width, 43));
            loadView.BackgroundColor = UIColor.Black;
            loadView.Alpha = 1;

            UIActivityIndicatorView load = new UIActivityIndicatorView(new CGRect(ViewInfoBeacon.Frame.Width / 2 - 17.5, 6, 35, 35));
            load.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.White;
            load.StartAnimating();

            loadView.Add(load);
            yy += 46;

            UIScrollView TextScroll = new UIScrollView(new CGRect(30, yy + 10, ViewInfoBeacon.Frame.Width - 60, ScrollHeight));

            NSAttributedStringDocumentAttributes opz = new NSAttributedStringDocumentAttributes();
            NSError err = new NSError();
            opz.DocumentType = NSDocumentType.HTML;
            opz.StringEncoding = NSStringEncoding.UTF8;

            NSAttributedString text = new NSAttributedString("<span style='color:white;font-size:15px'>" + oggetto.descrizione + "</span>", opz, ref err);

            size = UIStringDrawing.StringSize(oggetto.descrizione, UIFont.FromName("Montserrat-Light", 14), new CGSize(ViewInfoBeacon.Frame.Width - 60, 2000));

            UILabel Text = new UILabel(new CGRect(0, 0, ViewInfoBeacon.Frame.Width - 60, size.Height));
            Text.Font = UIFont.FromName("Montserrat-Light", 14);
            Text.TextAlignment = UITextAlignment.Justified;
            Text.TextColor = UIColor.White;
            Text.AttributedText = text;
            Text.Lines = 0;

            TextScroll.ContentSize = new CGSize(ViewInfoBeacon.Frame.Width - 60, size.Height);

            TextScroll.Add(Text);

            ViewInfoBeacon.Add(TitoloLabel);
            ViewInfoBeacon.Add(image);
            ViewInfoBeacon.Add(SeparatorView);
            ViewInfoBeacon.Add(play);
            ViewInfoBeacon.Add(pause);
            ViewInfoBeacon.Add(stop);
            ViewInfoBeacon.Add(loadView);
            ViewInfoBeacon.Add(TextScroll);

            ViewInfoBeacon.Alpha = 1;

            new System.Threading.Thread(new System.Threading.ThreadStart(() => {

                try
                {
                    string url = oggetto.urlaudio;
                    NSUrl fileURL = new NSUrl(url);
                    NSData soundData = NSData.FromUrl(fileURL);
                    NSError oe = new NSError();

                    try
                    {
                        audioPlayer = new AVAudioPlayer(data: soundData, fileTypeHint: "mp3", outError: out oe);
                    }
                    catch (Exception ee)
                    {
                        InvokeOnMainThread(() =>
                        {
                            new UIAlertView("Errore", "Errore nella riproduzione dell'audioguida", null, "Ok", null).Show();
                            Console.WriteLine("Error getting the audio file\n" + ee.StackTrace + "\n" + ee.Message);

                        });
                    }

                    if (audioPlayer != null)
                    {
                        InvokeOnMainThread(() => {

                            audioPlayer.PrepareToPlay();
                            audioPlayer.Volume = 1.0f;
                            audioPlayer.Delegate = this;
                            audioPlayer.Play();
                            audioPlayer.Pause();
                            IsPlaying = false;
                            loadView.Alpha = 0;

                            audioPlayer.FinishedPlaying += delegate
                            {
                                play.Image = play_NA;
                                pause.Image = pause_NA;
                                IsPlaying = false;
                            };

                        });
                    }
                }
                catch (Exception e)
                {
                    InvokeOnMainThread(() =>
                    {
                        new UIAlertView("Errore", "Errore nella riproduzione dell'audioguida", null, "Ok", null).Show();
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }

            })).Start();

        }


        public void CreatePaginaInfoImages(BeaconInfo oggetto)
        {

            foreach (var v in ViewInfoBeacon.Subviews)
            {
                v.RemoveFromSuperview();
            }

            ViewInfoBeacon.Alpha = 1;

            Back.Alpha = 1;

            isNOBEAC = false;
            isSTANZA = false;
            isBEAC = false;
            isInfoBEAC = true;


            audioPlayer = null;

            IsPlaying = false;

            UIImageView play, pause, stop;
            UIView loadView;

            int yy = 0;

            var size = UIStringDrawing.StringSize(oggetto.titolo, UIFont.FromName("Montserrat-Medium", 18), new CGSize(ViewInfoBeacon.Frame.Width, 2000));

            UILabel TitoloLabel = new UILabel(new CGRect(0, 12, ViewInfoBeacon.Frame.Width, size.Height));
            TitoloLabel.Font = UIFont.FromName("Montserrat-Medium", 18);
            TitoloLabel.TextAlignment = UITextAlignment.Center;
            TitoloLabel.TextColor = UIColor.White;
            TitoloLabel.Text = oggetto.titolo;
            TitoloLabel.Lines = 0;

            yy += (int)(size.Height + 15);

            float carouselHeight = (float)(((ViewInfoBeacon.Frame.Height - (size.Height + 90)) / 2) - 30);
            float ImageLato = (float)(carouselHeight - 2);
            float ScrollHeight = (float)(((ViewInfoBeacon.Frame.Height - (size.Height + 90)) / 2) + 30);

            UIScrollView horizontalScroll = new UIScrollView(new CGRect(0, yy + 5, View.Frame.Width, carouselHeight));

            int xx = 0;
            for (int i = 0; i < oggetto.ListUrlImage.Count; i++)
            {
                UIImageView img = new UIImageView(new CGRect(xx + 5, 0, ImageLato, ImageLato));
                img.Image = UIImage.FromFile("placeholder.png");

                //imageList.Add(img);

                xx += (int)(ImageLato + 5);

                ImageService.Instance
                   .LoadUrl(oggetto.ListUrlImage[i])
                   .LoadingPlaceholder("placeholder.png", ImageSource.CompiledResource)
                   .Into(img);

                horizontalScroll.Add(img);
            }

            horizontalScroll.ContentSize = new CGSize(xx + 5, carouselHeight);
            //UIImageView image = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - (ImageLato / 2), yy + 10, ImageLato, ImageLato));
            //image.Image = UIImage.FromFile("placeholder.png");

            //SetImageAsync(image, oggetto.urlImage);

            yy += (int)(carouselHeight + 10);

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;

            play = new UIImageView(new CGRect(ViewInfoBeacon.Frame.Width / 2 - 70, yy + 11, 35, 35));
            play.Image = play_NA;

            pause = new UIImageView(new CGRect(ViewInfoBeacon.Frame.Width / 2 - 17.5, yy + 11, 35, 35));
            pause.Image = pause_NA;

            stop = new UIImageView(new CGRect(ViewInfoBeacon.Frame.Width / 2 + 35, yy + 11, 35, 35));
            stop.Image = stopImm;

            UITapGestureRecognizer playTap = new UITapGestureRecognizer((s) =>
            {
                if (isHeadsetPluggedIn())
                {

                    if (!IsPlaying && audioPlayer != null)
                    {

                        audioPlayer.Play();
                        play.Image = play_A;
                        pause.Image = pause_NA;
                        IsPlaying = true;

                    }

                }
                else
                {
                    var alert = new UIAlertView("Errore", "Attaccare le cuffie per usufruire dell'audioguida", null, "Ok");
                    alert.Show();
                }
            });
            play.UserInteractionEnabled = true;
            play.AddGestureRecognizer(playTap);

            UITapGestureRecognizer pauseTap = new UITapGestureRecognizer((s) =>
            {

                if (IsPlaying && audioPlayer != null)
                {

                    audioPlayer.Pause();
                    play.Image = play_NA;
                    pause.Image = pause_A;
                    IsPlaying = false;

                }

            });
            pause.UserInteractionEnabled = true;
            pause.AddGestureRecognizer(pauseTap);

            UITapGestureRecognizer stopTap = new UITapGestureRecognizer((s) =>
            {
                if (audioPlayer != null)
                {

                    audioPlayer.Pause();
                    audioPlayer.CurrentTime = 0.0;
                    play.Image = play_NA;
                    pause.Image = pause_NA;
                    IsPlaying = false;

                }
            });
            stop.UserInteractionEnabled = true;
            stop.AddGestureRecognizer(stopTap);


            loadView = new UIView(new CGRect(0, yy + 5, ViewInfoBeacon.Frame.Width, 43));
            loadView.BackgroundColor = UIColor.Black;
            loadView.Alpha = 1;

            UIActivityIndicatorView load = new UIActivityIndicatorView(new CGRect(ViewInfoBeacon.Frame.Width / 2 - 17.5, 6, 35, 35));
            load.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.White;
            load.StartAnimating();

            loadView.Add(load);
            yy += 46;

            UIScrollView TextScroll = new UIScrollView(new CGRect(30, yy + 10, ViewInfoBeacon.Frame.Width - 60, ScrollHeight));

            string a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            NSAttributedStringDocumentAttributes opz = new NSAttributedStringDocumentAttributes();
            NSError err = new NSError();
            opz.DocumentType = NSDocumentType.HTML;
            opz.StringEncoding = NSStringEncoding.UTF8;

            NSAttributedString text = new NSAttributedString("<span style='color:white;font-size:15px'>" + oggetto.descrizione + "</span>", opz, ref err);

            size = UIStringDrawing.StringSize(oggetto.descrizione, UIFont.FromName("Montserrat-Light", 14), new CGSize(ViewInfoBeacon.Frame.Width - 60, 2000));

            UILabel Text = new UILabel(new CGRect(0, 0, ViewInfoBeacon.Frame.Width - 60, size.Height));
            Text.Font = UIFont.FromName("Montserrat-Light", 14);
            Text.TextAlignment = UITextAlignment.Justified;
            Text.TextColor = UIColor.White;
            Text.AttributedText = text;
            Text.Lines = 0;

            TextScroll.ContentSize = new CGSize(ViewInfoBeacon.Frame.Width - 60, size.Height);

            TextScroll.Add(Text);

            ViewInfoBeacon.Add(TitoloLabel);
            ViewInfoBeacon.Add(horizontalScroll);
            ViewInfoBeacon.Add(SeparatorView);
            ViewInfoBeacon.Add(play);
            ViewInfoBeacon.Add(pause);
            ViewInfoBeacon.Add(stop);
            ViewInfoBeacon.Add(loadView);
            ViewInfoBeacon.Add(TextScroll);

            Console.WriteLine("Headset attached:" + isHeadsetPluggedIn());

            new System.Threading.Thread(new System.Threading.ThreadStart(() => {

                try
                {
                    string url = oggetto.urlaudio;
                    NSUrl fileURL = new NSUrl(url);
                    NSData soundData = NSData.FromUrl(fileURL);
                    NSError oe = new NSError();

                    try
                    {
                        audioPlayer = new AVAudioPlayer(data: soundData, fileTypeHint: "mp3", outError: out oe);
                    }
                    catch (Exception ee)
                    {
                        InvokeOnMainThread(() =>
                        {
                            new UIAlertView("Errore", "Errore nella riproduzione dell'audioguida", null, "Ok", null).Show();
                            Console.WriteLine("Error getting the audio file\n" + ee.StackTrace + "\n" + ee.Message);

                        });
                    }

                    if (audioPlayer != null)
                    {
                        InvokeOnMainThread(() => {

                            audioPlayer.PrepareToPlay();
                            audioPlayer.Volume = 1.0f;
                            audioPlayer.Delegate = this;
                            audioPlayer.Play();
                            audioPlayer.Pause();
                            IsPlaying = false;
                            loadView.Alpha = 0;

                            audioPlayer.FinishedPlaying += delegate
                            {
                                play.Image = play_NA;
                                pause.Image = pause_NA;
                                IsPlaying = false;
                            };

                        });
                    }
                }
                catch (Exception e)
                {
                    InvokeOnMainThread(() =>
                    {
                        new UIAlertView("Errore", "Errore nella riproduzione dell'audioguida", null, "Ok", null).Show();
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }

            })).Start();

        }

        public void CreatePaginaInfoVideo(BeaconInfo oggetto)
        {

            foreach (var v in ViewInfoBeacon.Subviews)
            {
                v.RemoveFromSuperview();
            }

            Back.Alpha = 1;

            isNOBEAC = false;
            isSTANZA = false;
            isBEAC = false;
            isInfoBEAC = true;

            int yy = 0;

            isPlay = false;

            var size = UIStringDrawing.StringSize(oggetto.titolo, UIFont.FromName("Montserrat-Medium", 18), new CGSize(ViewInfoBeacon.Frame.Width, 2000));

            UILabel TitoloLabel = new UILabel(new CGRect(0, 12, ViewInfoBeacon.Frame.Width, size.Height));
            TitoloLabel.Font = UIFont.FromName("Montserrat-Medium", 18);
            TitoloLabel.TextAlignment = UITextAlignment.Center;
            TitoloLabel.TextColor = UIColor.White;
            TitoloLabel.Text = oggetto.titolo;
            TitoloLabel.Lines = 0;

            yy += (int)(size.Height + 15);

            float VideoHeight = (float)(((ViewInfoBeacon.Frame.Height - (size.Height + 45)) / 2) - 30);
            float ScrollHeight = (float)(((ViewInfoBeacon.Frame.Height - (size.Height + 45)) / 2) + 30);

            player = new MPMoviePlayerController(new NSUrl(oggetto.videoUrl))
            {
                AllowsAirPlay = true,
                Fullscreen = true,
                ScalingMode = MPMovieScalingMode.AspectFit,
                RepeatMode = MPMovieRepeatMode.One
            };
            player.ControlStyle = MPMovieControlStyle.None;
            player.View.Frame = new CGRect(0, yy + 10, ViewInfoBeacon.Frame.Width, VideoHeight);
            player.PrepareToPlay();
            player.ShouldAutoplay = false;


            UIView TrasparentClickView = new UIView(new CGRect(0, yy + 10, ViewInfoBeacon.Frame.Width, VideoHeight));
            TrasparentClickView.BackgroundColor = UIColor.Clear;

            UITapGestureRecognizer VideoTap = new UITapGestureRecognizer((s) =>
            {
                if (!isPlay)
                {
                    if (isHeadsetPluggedIn())
                    {
                        player.SetFullscreen(true, true);
                        //player.ControlStyle = MPMovieControlStyle.Embedded;
                    }
                    else
                    {
                        var alert = new UIAlertView("Errore", "Attaccare le cuffie per usufruire dell'audioguida", null, "Ok");
                        alert.Show();
                    }
                }
            });
            TrasparentClickView.UserInteractionEnabled = true;
            TrasparentClickView.AddGestureRecognizer(VideoTap);

            //player.Play();

            yy += (int)(VideoHeight + 10);

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;


            UIScrollView TextScroll = new UIScrollView(new CGRect(30, yy + 11, ViewInfoBeacon.Frame.Width - 60, ScrollHeight));

            string a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            NSAttributedStringDocumentAttributes opz = new NSAttributedStringDocumentAttributes();
            NSError err = new NSError();
            opz.DocumentType = NSDocumentType.HTML;
            opz.StringEncoding = NSStringEncoding.UTF8;

            NSAttributedString text = new NSAttributedString("<span style='color:white;font-size:15px'>" + oggetto.descrizione + "</span>", opz, ref err);

            size = UIStringDrawing.StringSize(oggetto.descrizione, UIFont.FromName("Montserrat-Light", 14), new CGSize(ViewInfoBeacon.Frame.Width - 60, 2000));

            UILabel Text = new UILabel(new CGRect(0, 0, ViewInfoBeacon.Frame.Width - 60, size.Height));
            Text.Font = UIFont.FromName("Montserrat-Light", 14);
            Text.TextAlignment = UITextAlignment.Justified;
            Text.TextColor = UIColor.White;
            Text.AttributedText = text;
            Text.Lines = 0;

            TextScroll.ContentSize = new CGSize(ViewInfoBeacon.Frame.Width - 60, size.Height);

            TextScroll.Add(Text);

            ViewInfoBeacon.Add(TitoloLabel);
            ViewInfoBeacon.Add(player.View);
            ViewInfoBeacon.Add(TrasparentClickView);
            ViewInfoBeacon.Add(SeparatorView);
            ViewInfoBeacon.Add(TextScroll);
        }



        // VERIFICARE SE CI SONO CUFFIE
        public bool isHeadsetPluggedIn()
        {
            AVAudioSessionRouteDescription route = AVAudioSession.SharedInstance().CurrentRoute;
            foreach (AVAudioSessionPortDescription descr in route.Outputs)
            {
                Console.WriteLine(descr.PortType);
                if (descr.PortType.IsEqual(new NSString("Headphones")))
                    return true;
            }
            return false;
        }

    }


    public class CarouselDataSource : iCarouselDataSource
    {
        int page = 3;
        ViewController Super;

        public CarouselDataSource(ViewController s)
        {
            // create our amazing data source
            Super = s;
        }

        // let the carousel know how many items to render
        public override nint GetNumberOfItems(iCarousel carousel)
        {
            // return the number of items in the data
            return  page;
        }

        // create the view each item in the carousel
        public override UIView GetViewForItem(iCarousel carousel, nint index, UIView view)
        {

            UIView View = null;

            Console.WriteLine("IND:" + index +"|"+(view == null));
            if (view == null)
            {

                if (index == 0)
                    View = Super.ViewVideo;

                if (index == 1)
                    View = Super.ViewMain;

                if (index == 2)
                    View = Super.ViewSito;
            }
            else
            {
                View = view;
            }

            return View;
        }
    }




}